FROM debian:buster

# Installing necessary packages
RUN	apt-get update && apt-get upgrade -y && apt-get install -y \
	mariadb-server \
	nginx \
	openssl \
	php7.3 \
	php7.3-bcmath php7.3-bz2 php7.3-fpm php7.3-gd php7.3-intl php7.3-mbstring php7.3-mysql php7.3-xml php7.3-zip \
	wget

# Retrieving wordpress and phpmyadmin sources
RUN	mkdir -p /var/www/html \
	&& wget -qO- https://wordpress.org/latest.tar.gz \
	| tar xz -C /var/www/html/ --one-top-level=wordpress --strip-components 1 \
	&& wget -qO- https://files.phpmyadmin.net/phpMyAdmin/5.1.0/phpMyAdmin-5.1.0-all-languages.tar.gz \
	| tar xz -C /var/www/html/ --one-top-level=phpmyadmin --strip-components 1

# Setting autorisations for phpmyadmin cache
RUN	mkdir /var/www/html/phpmyadmin/tmp \
	&& chown www-data:www-data /var/www/html/phpmyadmin/tmp \
	&& chmod 700 /var/www/html/phpmyadmin/tmp

# SSL config
RUN	 openssl req -x509 -nodes -days 365 -newkey rsa:2048 -sha256 \
	-keyout /etc/ssl/self-signed.key -out /etc/ssl/self-signed.crt \
	-subj "/C=FR/ST=AURA/L=Lyon/O=42School/OU=wetieven/CN=localhost"

# Creating an admin account to use our sql DB
RUN	service mysql start \
	&& echo "CREATE USER admin@localhost IDENTIFIED BY 'guest';" | mysql -u root \
	&& echo "GRANT ALL PRIVILEGES ON *.* TO admin@localhost;" | mysql -u root \
# Creating a user account to use our Wordpress
	&& echo "CREATE DATABASE wordpress;" | mysql -u root \
	&& echo "CREATE USER user@localhost IDENTIFIED BY 'password';" | mysql -u root \
	&& echo "GRANT ALL PRIVILEGES ON wordpress.* TO user@localhost;" | mysql -u root

# Retrieving our config files and scripts
COPY srcs/nginx_conf /etc/nginx/sites-available/default
COPY srcs/phpmadm_conf.php /var/www/html/phpmyadmin/config.inc.php
COPY srcs/wp_conf.php /var/www/html/wordpress/wp-config.php
COPY srcs/index_toggle.sh /index_toggle.sh

# Launching our server
CMD service php7.3-fpm start && service mysql start && nginx -g "daemon off;"

# Providing the ports susceptible to be used by our container
EXPOSE 80
EXPOSE 443
