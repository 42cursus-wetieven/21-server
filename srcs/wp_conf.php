<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'user' );

/** MySQL database password */
define( 'DB_PASSWORD', 'password' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'XLw|7+M^&;A9=M] r)EYg@N5=^Woemq-B%bT}m ;3>Ddoppe#>^|4-F(~Nq$AO8+');
define('SECURE_AUTH_KEY',  '[|wO1-YNE+!#G593m+k~Dw`D(xk#*H-lwhXX6MLuu}kQkk7cWkiis=H-XYfROoto');
define('LOGGED_IN_KEY',    ';G*b29kV(abV|+_m)6QLx8=xlA;5x5Tp)PjO%@dJWh>$<,3i<WFmmh|?{4hqg}Ww');
define('NONCE_KEY',        '`Wx_D3osh7G+bxAV.L[f)K;#($,M~.e)D&5R%eK2~eD!TK11STa#uXLL3E=Z 1ej');
define('AUTH_SALT',        'bEIy:]`>-20Mvh;pzE/+[Y3qd|n!lxpW+Xg(vmW?,<!V,A0`f%r.3|#-.YC9I[:g');
define('SECURE_AUTH_SALT', '>Yl`s;vSz!4!(R+f-3oZLY&V^#H]l?/ZP(H#jzS<4}<StGB2_pPpkCp)yA7-ogo ');
define('LOGGED_IN_SALT',   'x0SbtkEGdRcMQALp;Qah7`&1uxzKqdBYmjgwrcT5]]mTefYxRP|L}[F*/gZrc|(I');
define('NONCE_SALT',       '+g[^ubrl-MzuuP%bg h.$kJ$|$~}K6bX$I-_m$ItOJ}g+4k2|?BTGe-9/><Wt:q ');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
