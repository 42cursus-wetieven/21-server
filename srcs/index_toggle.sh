#!/bin/bash
if [ "$1" == "enable" ];
then
		sed -i -e 's/autoindex off/autoindex on/g' /etc/nginx/sites-available/default
        echo "autoindex has been enabled."
elif [ "$1" == "disable" ];
then
		sed -i -e 's/autoindex on/autoindex off/g' /etc/nginx/sites-available/default
        echo "autoindex has been disabled."
else
        echo "usage: $0 enable/disable."
		exit 2
fi
nginx -s reload
