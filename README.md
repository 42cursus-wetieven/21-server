# ft_server

A basic web server deployed via Dockerfile

## Build

* docker build -t ft_server .

## Run

* docker run --rm -d -p 80:80 -p 443:443 --name=fts ft_server

## Exec

* docker exec -ti fts bash

## Autoindex toggle

* docker exec -t fts bash -c "bash /index_toggle.sh disable"
* docker exec -t fts bash -c "bash /index_toggle.sh enable"
